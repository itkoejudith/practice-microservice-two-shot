from .keys import PEXELS_API_KEY
import requests
import json


# pick the params you want the picture to show
def get_photo(style_name, color):
    params = {
        "per_page": 1,
        "query": style_name + " " + color,
    }
    headers = {"AUTHORIZATION": PEXELS_API_KEY}
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    picture_url = content["photos"][0]["src"]["original"]
    try:
        return {"picture_url": picture_url}
    except (KeyError, IndexError):
        return {"picture_url": None}
