from django.urls import path

from .views import (
    api_show_hat,
    api_list_hats,
    api_locations,
)

urlpatterns = [
    path("hats/<int:id>/", api_show_hat, name="api_hat"),
    path("hats/", api_list_hats, name="api_hats"),
    path("locations/", api_locations, name="api_locations"),
]
