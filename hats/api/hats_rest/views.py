from django.shortcuts import render
from .models import Hats, LocationVO
from django.http import JsonResponse
from .acls import get_photo
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json


# Create your views here.
class LocationVODetailEncoder(ModelEncoder):
    properties = [
        "closet_name",
        "section_number",
        "shelf_number",
        "id",
    ]


class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "style_name",
        "color",
        "picture_url",
        "location",
        "id",
    ]
    encoders = {"location": LocationVODetailEncoder()}


class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "loctaion",
        "id",
    ]

    encoders = {"location": LocationVODetailEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatsListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            location = LocationVO.objects.get(id=content["location"])
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        photo = get_photo(content["style_name"], content["color"])
        content.update(photo)
        print(content["location"])
        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_hat(request, id):
    try:
        hat = Hats.objects.get(id=id)
    except Hats.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid hat id"},
            status=400,
        )
    if request.method == "GET":
        hat = Hats.objects.get(id=id)
        return JsonResponse(
            {"hat": hat},
            encoder=HatsDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hats.objects.filters(id - id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = LocationVO.objects.values(id=content["location"])
                content["location"] = location
        except location.DoesNotExist:
            return JsonResponse({"message": "Invalid location"}, status=400)
        Hats.objects.filter(id=id).update(**content)

        return JsonResponse(hat, encoder=HatsDetailEncoder, safe=False)


@require_http_methods(["GET", "POST"])
def api_locations(request):
    if request.method == "GET":
        locations = LocationVO.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationVODetailEncoder,
        )
    else:
        content = json.loads(request.body)
        location = LocationVO.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationVODetailEncoder,
            safe=False,
        )
