from django.contrib import admin

# Register your models here.
from .models import LocationVO, Hats


@admin.register(LocationVO)
class LocationVOAdmin(admin.ModelAdmin):
    pass


@admin.register(Hats)
class HatsAdmin(admin.ModelAdmin):
    pass
