from django.apps import AppConfig


# go to settings.py of shoes project
class ShoesApiConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "shoes_rest"
