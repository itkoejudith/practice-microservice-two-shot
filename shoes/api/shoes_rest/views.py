# Import the required decorator and models
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from .models import Shoe, BinVO
import json
from common.json import ModelEncoder

# register the views!!!!!!! URL PATTERNS :D


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "image_url",
        "bin",
        "id",
    ]

    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "image_url",
        "bin",
        "id",
    ]

    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}


# Define the view function and allow GET and POST requests
@require_http_methods(["GET", "POST"])
def list_shoes(request, bin_vo_id=None):
    # If the request method is GET
    if request.method == "GET":
        # If a bin ID is provided, filter shoes by bin ID
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        # Otherwise, return all shoes
        else:
            shoes = Shoe.objects.all()
        # Return a JSON response containing the shoes, encoded using a custom encoder
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    # If the request method is POST
    else:  # POST
        # Parse the request body as JSON
        content = json.loads(request.body)
        try:
            # Get the BinVO object with the specified bin ID
            bin_href = f'/api/bins/{content["bin"]}/'
            bin = BinVO.objects.get(import_href=bin_href)
            # Replace the bin ID in the content with the BinVO object
            content["bin"] = bin
        except BinVO.DoesNotExist:
            # If the BinVO object does not exist, return an error message
            return JsonResponse({"error": "Bin does not exist SAD MESSAGE"}, status=400)
        # Create a new Shoe object with the parsed content and the BinVO object as its bin attribute
        shoe = Shoe.objects.create(**content)
        # Return a JSON response containing the newly created Shoe object, encoded using the same custom encoder
        return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False)


@require_http_methods(["GET", "DELETE"])
def shoe_details(request, id):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=id)
            return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False)
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Yup this is totally wrong... nice try."})
    else:
        count, _ = Shoe.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
