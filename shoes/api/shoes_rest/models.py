from django.db import models
from django.urls import reverse


# Create your models here.
class BinVO(models.Model):
    import_href = models.CharField(max_length=100, unique=True)  # href for the path
    closet_name = models.CharField(max_length=100)  # dropdown what name to save it on

    def __str__(self):  # this gives the closet name instead of bin/object number
        return f"{self.closet_name}"


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    image_url = models.URLField()
    bin = models.ForeignKey(BinVO, related_name="shoes", on_delete=models.CASCADE)

    def get_api_url(self):
        return reverse("shoe_detail", kwargs={"id": self.id})


# GET ID FROM SHOE DETAIL, RELATION TO VIEW FUNCTION
# REMEMBER TO REGISTER THESE MODELS IN THE ADMIN
